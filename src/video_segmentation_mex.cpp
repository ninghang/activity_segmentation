#include "constants.h"
#include "readData.cpp"
#include "segmentation_skel.cpp"
#include <iostream>
#include <fstream>

#include "mex.h"

std::ofstream segmentfile;

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    srand(time(0));

    // string dataLocation = mxArrayToString(prhs[0]);
    // string video_id = mxArrayToString(prhs[1]);
    int method = (int) mxGetScalar(prhs[2]);
    float threshold = (float) mxGetScalar(prhs[3]);
    string transformfile = mxArrayToString(prhs[4]);

    // make sure it's a row vector
    const mxArray *DataCell = prhs[0];
    const mwSize num_dataCell = mxGetM(DataCell);

    const mxArray *DataCellPos = prhs[1];
    const mwSize num_dataCellPos = mxGetM(DataCellPos);

    // dataLocation = dataLocation + "/";

    double **data; //[JOINT_NUM][JOINT_DATA_NUM];
    int **data_CONF; //[JOINT_NUM][JOINT_DATA_TYPE_NUM]
    double **pos_data; //[POS_JOINT_NUM][POS_JOINT_DATA_NUM];
    int *pos_data_CONF; //[POS_JOINT_NUM]
    data = new double*[JOINT_NUM];
    data_CONF = new int*[JOINT_NUM];
    for (int i = 0; i < JOINT_NUM; i++) {
        data[i] = new double[JOINT_DATA_NUM];
        data_CONF[i] = new int[JOINT_DATA_TYPE_NUM];
    }
    pos_data = new double*[POS_JOINT_NUM];
    pos_data_CONF = new int[POS_JOINT_NUM];
    for (int i = 0; i < POS_JOINT_NUM; i++) {
        pos_data[i] = new double[POS_JOINT_DATA_NUM];
    }

    double **data2; //[JOINT_NUM][JOINT_DATA_NUM];
    int **data_CONF2; //[JOINT_NUM][JOINT_DATA_TYPE_NUM]
    double **pos_data2; //[POS_JOINT_NUM][POS_JOINT_DATA_NUM];
    int *pos_data_CONF2; //[POS_JOINT_NUM]
    data2 = new double*[JOINT_NUM];
    for (int i = 0; i < JOINT_NUM; i++) {
        data2[i] = new double[JOINT_DATA_NUM];
    }
    pos_data2 = new double*[POS_JOINT_NUM];
    for (int i = 0; i < POS_JOINT_NUM; i++) {
        pos_data2[i] = new double[POS_JOINT_DATA_NUM];
    }

    // readData* DATA = new readData(dataLocation, video_id);
    Segmentation_skel segSkel;

    mxArray *frame_data;
    mxArray *frame_pos_data;
    mwSize nrows_data;
    mwSize nrows_data_pos;
    double *array_data;
    double *array_data_pos;


    // while (status > 0)
    for (size_t i = 0; i < num_dataCell; i++)
    {

        frame_data = mxGetCell(DataCell, i);
        frame_pos_data = mxGetCell(DataCellPos, i);
        nrows_data = mxGetM(frame_data);
        nrows_data_pos = mxGetM(frame_pos_data);
        array_data = mxGetPr(frame_data);
        array_data_pos = mxGetPr(frame_pos_data);

        // DATA->readNextFrame(data, pos_data, data_CONF, pos_data_CONF);
        for (size_t jn = 0; jn < JOINT_NUM; ++jn)
        {
            for (size_t jdn = 0; jdn < JOINT_DATA_NUM; ++jdn)
            {
                data2[jn][jdn] = array_data[jn+jdn*nrows_data];
                // if (data2[jn][jdn] != data[jn][jdn])
                // {
                //     mexPrintf("ERROR%f,%f\n",data2[jn][jdn],data[jn][jdn]);
                //     return;
                // }
            }
        }
        for (size_t pjn = 0; pjn < POS_JOINT_NUM; ++pjn)
        {
            for (size_t pjdn = 0; pjdn < POS_JOINT_DATA_NUM; ++pjdn)
            {
                pos_data2[pjn][pjdn] = array_data_pos[pjn+pjdn*nrows_data_pos];
                // if (pos_data2[pjn][pjdn]!=pos_data[pjn][pjdn])
                // {
                //     mexPrintf("%f,%f\n",pos_data2[pjn][pjdn],pos_data[pjn][pjdn]);
                //     return;
                // }

            }
        }

        segSkel.addSkelFrame(data2, pos_data2, transformfile);
        // cout << "status = " << status << endl;

    }

    //segSkel.computeSegments();

    segSkel.computeSegmentsDynamic(threshold,method);
    // segmentfile << video_id << ";";
    // segSkel.printSegments(segmentfile);

    vector<double> v = segSkel.outputSegments();
    plhs[0] = mxCreateDoubleMatrix(1,v.size(),mxREAL);
    double *output_pointer = mxGetPr(plhs[0]);
    for (vector<double>::size_type ii = 0; ii < v.size();++ii)
    {
        output_pointer[ii] = v[ii];
        // mexPrintf("%d,",(int)v[ii]);
    }
    // }
    // segmentfile.close();
}
/**
   there are 11 joints that have both orientation (3x3) and position (x,y,z) data
   XN_SKEL_HEAD,0
   XN_SKEL_NECK,1
   XN_SKEL_TORSO,2
   XN_SKEL_LEFT_SHOULDER,3
   XN_SKEL_LEFT_ELBOW,4
   XN_SKEL_RIGHT_SHOULDER,5
   XN_SKEL_RIGHT_ELBOW,6
   XN_SKEL_LEFT_HIP,7
   XN_SKEL_LEFT_KNEE,8
   XN_SKEL_RIGHT_HIP,9
   XN_SKEL_RIGHT_KNEE,10

   there are 4 joints that have only position (x,y,z) data
   XN_SKEL_LEFT_HAND,11
   XN_SKEL_RIGHT_HAND,12
   XN_SKEL_LEFT_FOOT,13
   XN_SKEL_RIGHT_FOOT,14

   data[][0~8]    -> orientation (3x3 matrix)
   3x3 matrix is stored as
   0 1 2
   3 4 5
   6 7 8
   read PDF for description about 3x3 matrix
   data[][9~11]   -> x,y,z position for eleven joints

   data_CONF[][0]   -> confidence value of orientation  (data[][0~8])
   data_CONF[][1]   -> confidence value of xyz position (data[][9~11])

   data_pos[][0~2] -> x,y,z position for four joints
   data_pos_CONF[]  -> confidence value of xyz position (data_pos[][0~2])

   X_RES and Y_RES are in constants.h, so just use them.
   IMAGE[X_RES][Y_RES][0~2]   -> RGB values
   IMAGE[X_RES][Y_RES][3]     -> depth values


*/
