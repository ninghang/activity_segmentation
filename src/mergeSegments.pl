#!/usr/bin/perl -w
# quit unless we have the correct number of command-line args

$num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage: merge_segment <segmentation file>\n";
    exit;
}

print "\nMerging segments that have less than 4 frames...\n";

$segmentation_file=$ARGV[0];
unless (-e $segmentation_file)
{
    print "Error: Cannot find the segmentation file\n";
    exit;
}

#### Merging segments
open(OF,">Segmentation_merged.txt");

%segCount = ();
open (F,$segmentation_file);
while(<F>){
    chomp();
    %fmap = ();
    ($id, @a ) = split/;/, $_;
    print OF "$id;";
    $segCount{$id} = scalar @a;
    $maxsid = 0;
    for $s (@a){
        ($sid , $frames) = split/:/, $s;
        (@f) = split/,/, $frames;
        $fmap{$sid} = [@f];
        if($sid > $maxsid) {$maxsid = $sid;}
    }
    for ($k = 2; $k <= $maxsid; $k++){
        if(exists $fmap{$k}){
            if(scalar @{$fmap{$k}} < 4){
                # merge this segment to the previous segment
                $p = $k;
                do{
                    $p = $p-1;
                } while ($p>0 && !exists $fmap{$p});
                if (exists $fmap{$p}){
                    push (@{$fmap{$p}}, @{$fmap{$k}});
                    delete $fmap{$k};
                }
                #     print "deleted : $id,$k\n";

            }
        }
    }
    # also merge segment 1 to later segment
    if(exists $fmap{1} && scalar @{$fmap{1}} < 4){
        $n = 1;
        do{
            $n = $n+1;
        } while ($n<= $maxsid && !exists $fmap{$n});
        if (exists $fmap{$n}){
            unshift (@{$fmap{$n}}, @{$fmap{1}});
            delete $fmap{1};
        }
    }
    for $k (sort {$a<=>$b} keys %fmap ){
        print  OF "$k:$fmap{$k}[0]";
        @a = @{$fmap{$k}};
        for($i = 1; $i <= $#a; $i++ ){
            print OF ",$a[$i]";
        }
        print OF  ";"
    }

    print OF "\n";
}
close(F);
close(OF);

print "\nDone.\n"
