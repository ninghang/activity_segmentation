TARGET:= video_segmentation
TARGET_MEX:= video_segmentation_mex
#PCL_INCLUDE:= /opt/ros/groovy/include/pcl-1.6

cpp	:
	g++ src/${TARGET}.cpp -o build/${TARGET} -Iincludes -lboost_iostreams
	@echo
	@echo Done.

mex 	:
	mex src/${TARGET_MEX}.cpp -o build/${TARGET} -Iincludes -lboost_iostreams
	@echo
	@echo Done.

all	: cpp mex

clean	:
	rm -f build/${TARGET}.mexa64
	rm -f build/${TARGET}
