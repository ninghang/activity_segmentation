clear all

clc

addpath('../build')

file = '../data/1204142227.txt';

transformation_file = '../data/globalTransform.txt';

skeletons = read_skeleton_from_file(file);

segmentation = video_segmentation(skeletons(:,1),skeletons(:,2), 1, 500,'');

[merged_segmentation,segmentation] = split_merge_segments(segmentation);

disp(merged_segmentation)