function skeletons = read_skeleton_from_file(fileName)

database = importdata(fileName);
nrFrames = size(database,1);
skeletons = cell(nrFrames,2);

for r = 1 : nrFrames
    
    frame = database(r,2:end);
    data = frame(1:11*14);
    data = reshape(data,14,11)';
    
    data_pos = frame(11*14+1:end);
    data_pos = reshape(data_pos,4,4)';

    data(:,[10,14]) = [];
    data_pos(:,4) = [];
    
    skeletons{r,1} = data;
    skeletons{r,2} = data_pos;
    
end

end