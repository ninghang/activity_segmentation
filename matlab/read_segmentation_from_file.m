function s = read_segmentation_from_file(segmentation_file)

fileID = fopen(segmentation_file);
str = textscan(fileID,'%s');
str = str{1}{1};
fclose(fileID);

[seg_start,seg_end] = regexp(str, '[0-9]*;');

s = zeros(1,length(seg_start)-1);

for i = 2 : length(seg_start)
    
    a = str(seg_start(i):seg_end(i));
    s(i-1) = str2double(a(1:end-1)); % remove semi-colon
    
end
s = diff([0,s]);

end