# Activity Segmentation

This package is for video temporal segmentation based on the skeleton positions descripted in [1] and [2].

## Dependencies

    make g++ libboost_iostreams

## Compile

Compile the executable

    mkdir build
    make

Compile the Matlab interface

    mkdir build
    make mex

Clean files

    make clean

- - -

## Use Executable in Bash Command

The syntax of usage:

    video_segmentation <data directory> <skeleton file> <method> <threshold>

Example:

    ./build/video_segmentation data 1204142227 1 500

Then we may want to merge the small segments with the others

    perl ./src/mergeSegments.pl Segmentation.txt

### Input

`data directory` : directory that contains the skeleton file and the corresponding data transformation file

`method` : takes value 1 or 2

`threshold` : 100, 500 and 1000 were used for the results reported in [1]

`skeleton file` : Skeleton Data Format:

The skeleton data consists of 15 joints. There are 11 joints that have both joint orientation and joint position. And, 4 joints that only have joint position.

Each row has the following format:

    Frame#,ORI(1),P(1),ORI(2),P(2),...,P(11),J(11),P(12),...,P(15)

    Frame# => integer starting from 1

    ORI(i) => orientation of ith joint
                0 1 2
                3 4 5
                6 7 8
              3x3 matrix is stored as followed by CONF
                0,1,2,3,4,5,6,7,8,CONF
              Read NITE PDF (see below) to get more detail about the matrix

    P(i)   => position of ith joint followed by CONF
                x,y,z,CONF
              values are in milimeters

    CONF   => boolean confidence value (0 or 1)
              Read NITE PDF (see below) to get more detail about the confidence value

    Joint number -> Joint name
        1 -> HEAD
        2 -> NECK
        3 -> TORSO
        4 -> LEFT_SHOULDER
        5 -> LEFT_ELBOW
        6 -> RIGHT_SHOULDER
        7 -> RIGHT_ELBOW
        8 -> LEFT_HIP
        9 -> LEFT_KNEE
        10 -> RIGHT_HIP
        11 -> RIGHT_KNEE
        12 -> LEFT_HAND
        13 -> RIGHT_HAND
        14 -> LEFT_FOOT
        15 -> RIGHT_FOOT

### Output

The excutable outputs the segmentation file `Segmentation.txt`.

Format: <id>;<segment number>:<comma-seperated frame numbers>;<segment_number>:<comma-seperated frame numbers>;...

- - -

## Usage of Matlab Interface

The syntax of usage is:

    video_segmentation(<skeleton joints>,<location of hands and feet>,<method>,<threshold>,<transformation file>)

Example:

    cd matlab
    test_example


### Reference

[1] Learning Human Activities and Object Affordances from RGB-D Videos, Hema S Koppula, Rudhir Gupta, Ashutosh Saxena. International Journal of Robotics Research (IJRR) 2013.

[2] Efficient graph-based image segmentation, P. F. Felzenszwalb, D. Huttenlocher. IJCV, 2004